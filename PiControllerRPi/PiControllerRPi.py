import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

GPIO.setup(24, GPIO.OUT)
GPIO.setup(22, GPIO.OUT)

GPIO.output(22, GPIO.HIGH)
GPIO.output(24, GPIO.HIGH)
